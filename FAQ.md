# FAQ

#### Cos'è PrivaSì? 
PrivaSì è un percorso creato da un gruppo di ragazzi volontari (Etica Digitale), con l'intento di sensibilizzare le singole persone riguardo l'uso e abuso dei dati al giorno d'oggi. Tramite semplici spiegazioni, di guide e, soprattutto, mostrando mezzi alternativi che rispettino la persona, si vuole dimostrare come si possa effettivamente riprendere in mano la propria privacy e, più in generale, la propria intimità.

#### Come posso sostenere il progetto?
In una maniera molto semplice ed economica: spargendo la voce. Non c'è cosa più utile, dato che è una cosa che riguarda tutti.  

#### Posso donare in qualche modo?
Nì: al momento non è possibile, ma daremo più info in seguito.

#### Come posso contribuire alla documentazione?
Se volete aggiungere o correggere qualcosa e non sapete come funziona GitLab, il modo più veloce è farsi un account e postare qui il vostro suggerimento: [Issues](https://gitlab.com/etica-digitale/privasi/issues) (inseriremo presto un form da seguire).  
Se invece sapete fare un fork e un merge request... beh, ottimo :D  
  
#### Perché la maggior parte delle fonti sono in inglese?
I contributi in lingua inglese sulla piazza sono più numerosi rispetto al materiale italiano, piuttosto scarno. Si tenga inoltre presente che spesso si usano board internazionali per seguire vari argomenti, dove gli articoli sono postati per l'appunto in inglese.  

[in allestimento]  
<br>
<br>
<br>
[Torna all'homepage](https://gitlab.com/etica-digitale/privasi)