# Conversazioni sensibili pt.1: Smart TV e assistenti vocali (Internet delle Cose)

> L'Internet delle Cose punta a conquistare la nostra vita: ogni apparecchio è "intelligente", comunica con i suoi simili e ci fa vivere come pascià; in tutto ciò, un'assistente vocale si prende cura di noi. Ma a quale prezzo?

## In parole semplici 
Una smart TV è una TV connessa a internet, solitamente dotata di un microfono. Se ne avete una vi invitiamo a dare un occhio al manuale delle istruzioni; perché, cercando bene, troverete un'avvertenza sul *non* parlare di argomenti sensibili nei pressi del televisore. Considerando che casa dovrebbe essere il nostro spazio più intimo e che la gente si autocensura quando sa di essere monitorata fino alla paranoia ("Ho detto qualcosa di male? Come si comporterebbe un innocente? Posso parlare di questa cosa? Devo far finta di niente? Esagero il mio comportamento per far capire che sono una brava persona? E ma se poi invece...?"; ci è venuta l'ansia a noi a scriverlo), vi lasciamo riflettere su quanto sia divertente che un elettrodomestico ci suggerisca di star attenti a ciò che diciamo nella nostra sfera privata.  
Quando ciò fece scalpore, nel 2015 Samsung si giustificò dicendo che lo faceva per migliorare la tecnologia¹. Tuttavia, nel 2017 WikiLeaks dimostrò come la CIA potesse sfruttare una falla proprio in quei televisori per monitorare le persone grazie al microfono incorporato nella TV². 

Se avessimo una TV non connessa a internet, per manometterla qualcuno dovrebbe fisicamente venire in casa nostra e metterci le mani. Avrebbe, quindi, ostacoli fisici (il cancello, gli occhi dei vicini, la porta e il tempo richiesto per pasticciare con la TV rimanendo nell'abitazione). Al contrario, con internet, tutto questo non è necessario: può essere manomesso in venti modi diversi a chilometri di distanza. E la cosa più grave è che, una volta trovata, la falla può essere sfruttata su tutti gli altri dispositivi uguali al nostro sparsi per il mondo. Questo vuol dire che *chiunque* con abbastanza conoscenze può metterci mano: che sia il vicino con manie di controllo, il governo o un gruppo di ragazzi annoiati poco importa, ciò che importa è che si hanno dei mezzi in casa che possono essere trasformati in cimici a costo zero.  
Ovviamente non vale solo per le TV, ma per qualsiasi cosa con un microfono e/o telecamera connesso a internet (ragion per cui nel percorso di PrivaSì spieghiamo passo passo come rendere sempre più sicuro il telefono. Ricordate la metafora dei legnetti?).

Lasciamo ora da parte la sicurezza. Ricordate perché Google voleva conoscerci meglio e migliorare la nostra esperienza? Ecco, allora probabilmente saprete rispondervi da soli al perché esistano gli assistenti vocali (Alexa, Cortana ecc.). E ricordiamoci che per potersi attivare con un comando vocale (generalmente il loro nome), hanno bisogno di stare *sempre* in ascolto. Una profilazione, quindi, non più sulla singola persona, ma dell'intero nucleo familiare.  

C'è inoltre chi potrebbe vederli come un compagno con cui parlare e combattere la solitudine. Tuttavia, non è con una macchina né *attraverso* una macchina che si sconfigge la solitudine: per esempio, intervistando gli streamer più influenti di Twitch (gente che fa dirette di ore e ore chiusa in camera, guadagnando interi stipendi e con tanti fan coi quali dialoga via chat nel mentre), è risultato come questi avessero problemi quali solitudine, ansia e depressione a causa del loro lavoro che li tiene costretti tra quattro mura per la maggior parte della giornata, senza altre persone se non quelle dietro allo schermo³.  

Infine, questo abuso di tecnologia in ogni ambito comporta il continuo delegare alla macchina, dove noi facciamo e ragioniamo sempre meno. Questo, soprattutto nei bambini, ha un impatto gravissimo perché li porta a esplorare sempre meno il mondo e, di conseguenza, non permette al cervello di svilupparsi come dovrebbe (si parla di *demenza digitale*)⁴.  

## Cosa fare  
C'è chi potrebbe reputare quest'opzione brutale, ma alla fine della giornata non ci sono alternative valide: sbarazzatevi di questi mezzi e non acquistatene di nuovi. Casa vostra è, appunto, vostra. E la vostra sfera privata anche.  

Qualche suggerimento:  
- Se pensate che questi mezzi facciano risparmiare tempo, provate a riflettere su cosa davvero vi ruba del tempo. Farsi del male perché qualcos'altro ci fa stare male, non sembra una buona soluzione.
- Se questi mezzi vi fanno sentire capiti e meno soli, provate a riflettere sul fatto che siano assistenti, fatti per assecondarvi. Se non avete amici, se state passando un brutto periodo, ricordatevi che siamo animali *sociali*. Provate a riallacciare i rapporti con qualcuno (non è detto che funzioni, ma l'importante è provare) o a prendere parte a qualche gruppo di volontariato nella vostra città: farete del bene a voi stessi *e* agli altri. E non abbiate paura a chiedere una mano a figure professionali come uno psicologo, perché a tutti può capitare di sentirsi soli.
- Se pensate di non volerli buttare perché ormai ci avete speso dei soldi, provate a riflettere su quanti soldi potrebbe farvi spendere il dovere rimediare *poi* ai problemi che possono causare. Senza contare che la salute non dovrebbe avere un prezzo.
- Se pensate che la casa diventi più intelligente, provate a riflettere se non vi stia rendendo a sua volta più pigri: meno facciamo/pensiamo e meno vogliamo fare/pensare. Ma è sia il fare che il pensare a renderci umani.  

E, anzi, a proposito di pensare, lo consigliamo proprio come esercizio di questo capitolo. Fatelo quando avete un attimo per voi stessi, tipo prima di andare a letto. Prendete un foglio/quaderno (basta che sia cartaceo) e una penna, e provate a rispondere a queste due domande: "Cosa mi ruba del tempo? Cosa mi fa stare bene?"  
Sembra facile, ma potreste sorprendere voi stessi :)  

(e non andate avanti prima di allora, o serve a poco)
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente  
[DA AGGIUNGERE: Man in the Middle/attacchi; supporto aggiornamenti breve rispetto a ciclo vita; dipendenza server]

## Approfondendo  
[DA AGGIUNGERE: sorveglianza di massa in Cina - credito sociale; caso Ring - Amazon; Alexa hotel Mariott; città di Toronto e Australia]
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Appendice
¹De Souza Ryan, [Be careful of what you say in front our Smart TV, warns Samsung](https://www.hackread.com/samsung-smart-tv-listening-conversations/), HackRead, 2016  
²Brandom Russel, [The CIA is hacking Samsung Smart TVs, according to WikiLeaks docs](https://www.theverge.com/2017/3/7/14841556/wikileaks-cia-hacking-documents-ios-android-samsung), The Verge, 2017  
³Glink, [The Dark Side of Streaming](https://invidio.us/watch?v=Iz81XKFOANI) (video), 2017  
⁴Spitzer Manfred, [Demenza Digitale](https://www.corbaccio.it/libri/demenza-digitale-9788867005512), Corbaccio, Milano, 2013