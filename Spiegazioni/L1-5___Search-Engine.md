# Motori di ricerca: al di là di Google

> Nel 2000 venivano fatte circa 33 milioni di ricerche al giorno su Google. Nel 2019, le ricerche giornaliere sono arrivate a ben 1000 *miliardi* (1.000.000.000.000)¹. I motori di ricerca sono ormai, quindi, parte del nostro quotidiano tanto quanto lavarci la faccia e mangiare. Ed esattamente come nel mangiare dovremmo prestare attenzione a cosa mangiamo per evitare problemi fisici, nel cercare su internet dovremmo prestare attenzione a cosa usiamo per evitare manipolazioni mentali.

## In parole semplici

Mettiamo che venga scoperta una nuova pianta: Tizio dice che ha proprietà benefiche e per questo va coltivata, Caio dice che uccide le piante a lei vicina e per questo va estirpata. Chi ha ragione?  

Nel mondo di oggi, bombardati costantemente da informazioni (giornali, TV, telefono, computer), le risposte più realistiche sono tre: o ce ne freghiamo perché abbiamo già mille altre cose a cui pensare, o facciamo una rapida ricerca su Google, oppure apriamo il primo link che ci capita a tiro sul nostro social preferito. In tutti e 3 i casi, quindi, non vogliamo davvero pensarci. O meglio, negli ultimi due casi, vogliamo pensarci due minuti, illuderci di esserci informati per bene, e usare quell'articolo che abbiamo letto (se ci ha convinto) come verità ultima. Tuttavia il problema è un altro: l'articolo che avete aperto è davvero il primo che avreste trovato o è il primo che *il mezzo* (Google, il social ecc.) vi voleva far trovare?  

No, niente complotti: algoritmi. Vi siete mai chiesti perché i post di alcune persone che avete tra gli amici su Facebook non appaiono mai sulla vostra home? Perché, semplicemente, Facebook non ritiene rilevanti i loro post per voi. E come fa a dirlo? Con la profilazione.  
Allo stesso modo, se qualcuno condivide un articolo su come la pianta sia il male e FB già sa che avete messo mi piace su articoli che elogiano la pianta, non si prenderà il disturbo di farvelo apparire sulla home. Questo è *gravissimo*. È gravissimo perché crea dei compartimenti stagni (per l'esattezza *bolle di filtraggio*, *filter bubble*) attorno alle persone, convincendole che il mondo *deve* andare come dicono loro, perché sulla loro home di Facebook e sulle ricerche di Google non vedono altro. Ognuno, in altre parole, pensa di detenere la verità, e le cose che scrolla ogni giorno sullo schermo del telefono ne sono la riprova. Ricordate il "personalizzare la vostra esperienza"? Eccovi la personalizzazione. E la morte del dialogo.  

Stesso discorso vale per la manipolazione generale dei risultati quando cerchiamo qualcosa: nel 2014 un'inchiesta di Yelp ha dimostrato come Google manipolasse le ricerche degli hotel², e Google stesso ammette di rimuovere i termini negativi nell'autocompilazione quando vogliamo cercare qualcosa. Prova del 9: scrivete "Hitler" nella barra di ricerca. Quanti termini "negativi" vi suggerisce (assassino, crimini, genocidio, guerra, morte ebrei ecc.)?  

Nel primo caso quello che sta facendo è abusare della sua posizione (e nulla gli vieta di farlo di nuovo dato che non ci sono leggi. Nel dettaglio in "Tecnicamente"), mentre nel secondo è *fingere* che non esistano cose negative a questo mondo. Ma se persino gli psicologi ci spiegano che non dovremmo celare la morte ai bambini³, per quale ragione dovremmo nascondere le cose negative a noi adulti? Il mondo non è rosa e fiori e censurare ciò che non vogliamo sentire (vero o meno che sia) non lo farà andare via.  

Quindi come difendersi da certi strumenti? Beh... si cambia strumento. Al contrario di quello che si pensa, ci sono molti altri motori di ricerca là fuori, e in "Cosa Fare" vi mostreremo pro e contro dei più rilevanti.

E a proposito della pianta: *se* hanno entrambi ragione, basta coltivarla lontana dalle altre piante. :)  

## Cosa fare
Come dicevamo ci sono vari motori di ricerca in circolazione. Quello che stiamo cercando è un motore che ci dia risultati pertinenti e che non ci usi per profilarci e/o portare acqua al suo mulino. Se cerchiamo X, deve essere X.  
Mettiamo le mani avanti dicendo che il motore di ricerca per eccellenza non esiste (che non vuol dire che siano tutti uguali, anzi), ed è per questo che ne abbiamo voluto consigliare (e consigliare un po' meno) più di uno: a seconda dei vostri scopi o eventuali limiti :)  
Una cosa deve però essere chiara: qualsiasi cosa scegliate da qua sotto, sappiate che a livello di privacy è meglio di Google.

#### DuckDuckGo

<div align="center"><img src="resources/L1-5 pic0.png"></div>    
Compagnia americana con una papera come icona, DuckDuckGo è senza ombra di dubbio l'alternativa più diffusa: le sue ricerche sono pertinenti, è facile e comodo da usare quanto Google, ha un'app tutta sua sia sul Play Store (Android) che App Store (Apple), è adottato da browser privati come Tor e non censura. O meglio, propone 3 livelli di ricerca -off, moderata, sicura- a seconda delle preferenze.  
Se non volete niente di complicato e volete evitare l'ennesima profilazione, questa papera è perfetta. Per usarla dal telefono, come già detto, trovate l'app nello store, mentre per usarla dal PC il sito ufficiale è https://duckduckgo.com/ .  
Una volta aperta da PC vi chiederà in automatico se volete installarla: cliccando su "Installa" (quella sotto è solo un'immagine a scopo illustrativo, non fa niente), da ora in poi qualsiasi cosa cercherete dalla barra di ricerca del vostro browser, sarà cercata con DuckDuckGo.  

<div align="center"><img src="resources/L1-5 pic1.png"></div>  

Ha inoltre una comoda funzione chiamata *bangs*. I bangs sono ricerche "speciali", che iniziano con un punto esclamativo, seguito da una parola chiave. Per esempio, cercando

> !w pinguino

cercherà su Wikipedia "pinguino", senza passare dalla pagina dei risultati. Perché "w" è la parola chiave associata a Wikipedia. Qui una lista: https://duckduckgo.com/bang?q=  
Si sottolinea tuttavia, che un bang non rende la ricerca privata, perché è come se steste aprendo Wikipedia e digitando "pinguino" nella loro barra.  

#### SearX

<div align="center"><img src="resources/L1-5 pic2.png"></div>  

Per gli utenti avanzati amanti del *self-hosting*, potete girare con le dovute precauzioni SearX sul vostro server domestico, scaricandolo dal loro [GitHub](https://github.com/asciimoo/searx). Se non sapete di cosa stiamo parlando, vi *s*consigliamo questa opzione.  

#### YaCy

<div align="center"><img src="resources/L1-5 pic3.png"></div>  
Il più particolare è sicuramente YaCy (letto "ya sii"). È particolare perché, al contrario di tutti gli altri, è P2P (*peer to peer*), ovvero non dipende da un server centrale bensì tutti i suoi utenti diventano mini server tra essi comunicanti. In altre parole, è decentralizzato e -letteralmente- nelle mani dei suoi utenti.  


Per spiegare come funziona, dobbiamo introdurre il concetto di *crawler*: vi siete mai chiesti come facciano i motori di ricerca a conoscere miliardi di pagine e suggerirvi poi quelle più opportune? È molto semplice: mandano un animaletto (digitale) a visitare una pagina, che è poi collegata a un'altra, e a un'altra, e così via. E mentre vaga per tutti i siti, fa una scansione di cosa c'è dentro. Il tutto nel giro di frazioni di secondi (!).  
Come fa quindi YaCy a scansionare internet se non c'è un server principale che manda questi animaletti? Beh... potete mandarli voi, sui siti che volete! E far usufruire agli altri utenti di YaCy i risultati del vostro animaletto. E viceversa. Insomma: l'unione fa la forza.  
YaCy, inoltre, offre due possibilità: l'uso condiviso della rete, che a forza di cose vi espone un minimo dandovi però accesso anche agli animaletti degli altri, e l'uso blindato, dove... beh, è 100% privacy, ma fate affidamento solo sul vostro.  

Per ora lasciamo YaCy a chi vuole smanettare e capisce l'inglese, perché servirebbe un capitolo intero solo per spiegarlo bene (e infatti lo rivedremo molto più avanti nel dettaglio). Questo è il sito: https://yacy.net/en/index.html e questi dei tutorial: https://invidio.us/channel/UCvy0FJxqOAlSZ2VXskej79Q . Se volete un'opzione semplice e comoda, tornate alla papera.

#### Motori meno consigliati

- **Qwant**: motore di ricerca dell'Unione Europea, riporta su un suo post del 2015 di avere i server forniti da Huawei⁴. Huawei è una compagnia cinese che, come ogni compagnia cinese che vuole sopravvivere, *deve* aderire al Partito Comunista⁵. Purtroppo, la Cina, la libertà e l'etica non vanno molto d'accordo, tanto che nel Paese sembra di vivere in un episodio di Black Mirror a causa del Sistema di Credito Sociale⁶ (e sembra che anche l'America ci stia facendo un pensierino⁷). Non sappiamo se i server siano ancora in collaborazione con Huawei dato che l'ultima notizia è del 2017, ma è meglio prestare attenzione.  
- **StartPage**: StartPage sfrutta il motore di ricerca di Google eliminando quelle parti che gli permettono di tracciare gli utenti. Tuttavia, se i risultati di Google vengono manipolati e/o censurati, di conseguenza anche quelli di StartPage lo saranno. Come pro, invece, è probabilmente il più blindato sulla piazza.  
- **Swisscows**: bunker svizzero dentro una montagna, tiene una linea di censura per via dei bambini sostenendo che internet dovrebbe essere "per tutta la famiglia"⁸. Tuttavia, guardando la questione alla radice, il problema non dovrebbe essere censurare internet per i bambini, bensì tenerli lontani da internet fino a una certa età. Con la stessa logica sennò, non si potrebbero mandare in onda i TG fino alle 22 perché i bambini sono svegli. Inoltre, sappiamo tutti quali sono i siti più visitati sulla rete...
- **Ecosia**: per quanto piantare alberi a seconda di quante ricerche si facciano e ad vengano cliccati suoni una bella iniziativa, questo motore di ricerca *green* lascia a desiderare sulla privacy: le ricerche vengono salvate permanentemente, condividono dati con terze parti e tracciano usando strumenti interni⁹. Di conseguenza sarebbe meglio contribuire all'ambiente in altri modi (per esempio evitando l'uso di plastica¹⁰, usando di più bici/mezzi pubblici o non sprecando cibo) ed evitare motori simili. 

#### Motori sconsigliati (ok, qua siamo a livelli Google)
- **Yandex**: un Google russo. Attualmente la loro pagina sulla privacy risulta inesistente (...)¹¹.
- **Bing**: motore di ricerca della Microsoft. Colleziona dati e li condivide con terze parti¹².
- **Yahoo**: passato sotto Verizon, compagnia telefonica americana che colleziona dati e li condivide con terze parti, anche il motore in sé dichiara di collezionarli e condividerli con esterni¹³.
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Tecnicamente
[DA AGGIUNGERE: filter bubble (test e risultati), assenza leggi, fingerprinting]

## Approfondendo
[DA AGGIUNGERE]  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Appendice
¹ nd, [How many Google searches per day on average in 2019?](https://ardorseo.com/blog/how-many-google-searches-per-day-2019/), Ardor Seo, 2019
² Edwards Jim, [Here's The Evidence That Google's Search Results Are Horribly Biased](https://www.businessinsider.com/evidence-that-google-search-results-are-biased-2014-10?IR=T), Business Insider, 2014  
³ nd, [“Coco”, i bambini e la morte](https://www.ilpost.it/2018/01/13/coco-parlare-di-morte-ai-bambini/), Il Post, 2018  
⁴ https://blog.qwant.com/qwant-rejoint-huawei-au-cebit-15-2/  
⁵ Yi-Zheng Lian, [China, the Party-Corporate Complex](https://www.nytimes.com/2017/02/12/opinion/china-the-party-corporate-complex.html), The New York Times, 2017  
⁶ Perroni Marta, [La Cina come Black Mirror: entro il 2020 introdurrà un punteggio sociale ](https://www.tpi.it/2018/03/20/cina-punteggio-sociale-black-mirror/), TPI News, 2019  
⁷ Elgan Mike, [Uh-oh: Silicon Valley is building a Chinese-style social credit system](https://www.fastcompany.com/90394048/uh-oh-silicon-valley-is-building-a-chinese-style-social-credit-system), Fast Company, 2019  
⁸ https://swisscows.ch/it/about#family-friendly  
⁹ https://www.reddit.com/r/privacy/comments/cvhfyw/noble_goals_but_ecosia_falls_short_in_their/  
¹⁰ McCallum Will, [Vivere senza plastica](https://www.harpercollins.it/9788869054846/vivere-senza-plastica/), Harper Collins, Milano, 2019  
¹¹ https://yandex.com/legal/privacy/  
¹² https://privacy.microsoft.com/it-it/privacystatement  
¹³ https://policies.oath.com/ie/it/oath/privacy/products/communications/index.html  