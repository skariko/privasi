# Introduzione: cosa me ne importa della privacy?

>  ATTENZIONE: il Livello 0 non comporta azioni da fare, bensì ha l'intento di gettare le basi per tutto il resto, con due letture. Non saltatelo!

“Cosa mi importa della privacy se non ho nulla da nascondere?”
<br>
Questa è la frase più comune quando si inizia a parlare di tutela della privacy.
Altri dubbi frequenti sono: “Ma tanto hanno già i miei dati, cosa mi cambia?” e la variante: “Tanto in qualche modo me li prendono comunque”. Analizziamoli insieme.
<br>

#### #1: Non ho nulla da nascondere
C'è un'azione che siamo soliti fare ogni giorno quando usciamo di casa o andiamo a letto: chiudiamo la porta. Perché, secondo voi, lo facciamo?<br>
Probabilmente, abbiamo anche una password/PIN/impronta digitale per sbloccare il telefono: di nuovo, perché lo facciamo? Fermatevi un attimo e provate a rispondere.
<br>
<br>
<br>
Se ci avete pensato, le opzioni sono due:<br>
o avete realizzato che siamo tutti criminali feccia della peggior specie (voi inclusi), oppure ci dà semplicemente fastidio che gli altri invadano la nostra sfera privata; che sia per derubarci o per guardare le nostre foto personali poco importa, sono comunque i "nostri spazi". Quelli, insomma, dove staccare la spina e non essere giudicati: per ridere, per piangere, per riflettere, per strimpellare uno strumento, per far versi idioti, per scambiarci effusioni, per dire cavolate con gli amici, persino per scoreggiare (*risate in sottofondo da sitcom*).<br>
<br>
Non è, quindi, un discorso sull'avere qualcosa da nascondere o meno. È un discorso delle sfaccettature che ci compongono: a lavoro ci comportiamo in un modo, con i parenti in un altro, con gli amici in un altro ancora e così via. È normale, e lo stesso discorso vale per le informazioni che condividiamo: non andremmo a condividere con il capo i discorsi più intimi fatti con gli amici (a meno che non sia anche lui nostro amico), o perlomeno li filtreremmo. Perché allora, dovremmo condividere tutti questi aspetti con un qualsiasi servizio online che fa incetta di dati -spesso per scopi pubblicitari-, permettendogli di ricostruire tutte le sfaccettature della nostra persona che non condivideremmo con nessuno o quasi?

La ragione, quindi, per la quale non lasciamo la porta di casa aperta, il telefono a portata di tutti e password in bella vista non è (solo) perché "non ho nulla da nascondere" (e anche lì, sfidiamo chiunque a distribuire le sue password in giro e a lasciare la porta aperta), bensì (anche) perché sono sfaccettature del nostro carattere che, per natura, *non vogliamo* condividere con tutti.<br>
<br>

#### #2: Tanto hanno già i miei dati
Mettiamo che qualcuno se ne approfitti di noi. La prima volta potremmo pensare che non l'ha fatto apposta, se siamo buoni anche la seconda e la terza. Se continua però, finirà per irritarci e arriverà il momento dove non ce la faremo più e diremo basta. È un po' lo stesso discorso: il fatto che qualcuno abbia già i nostri dati non implica che anche altri in futuro siano autorizzati ad averli. Soprattutto perché certi dati possono cambiare: se vi trasferiste, cambierebbe il dato "domicilio". Se cambiaste numero, cambierebbe il dato "recapito telefonico". O più semplicemente, anche i vostri gusti personali.<br>
È come decidere di andare a fare due passi lasciando legnetti come tracce: se a una certa smettessimo di lasciare legnetti, trovarci sarebbe molto più difficile e molti lascerebbero perdere perché lo sforzo non ne varrebbe la pena. 
<br>

### #3: In qualche modo me li prendono comunque
Probabile. Tuttavia c’è differenza tra l’essere l’1% protetti e esserlo il 99%. Torniamo alla porta: chiuderla a chiave riduce drasticamente le possibilità che uno sconosciuto ci entri in casa. Nessuno affermerebbe che, dato che c'è comunque una minima possibilità, chiudere la porta non ne vale la pena.
<br>
<br>
Questo percorso non vuole essere un percorso su come installare una super blindata di ultima generazione con venti serrature e chiavistelli (per poi magari lasciare la finestra aperta), bensì vuol far capire quanto avere una buona porta chiusa a chiave sia rispettoso sia nei propri confronti che in quelli di chi ci sta intorno (vedremo quest'ultimo punto più avanti). E che, a differenza della propria abitazione (uno spazio fisico delimitato), internet è un grosso spazio pubblico senza confini fisici basato su rapporti di fiducia.
<br>
<br>


[Continua: "Google - un esempio: vedere con i propri occhi i dati che abbiamo acconsentito di condividere"](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L0-1___Google.md) <br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)
