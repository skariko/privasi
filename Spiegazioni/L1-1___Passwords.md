# Password 101

> Cosa rende una password una buona password? E dove conviene custodirle per tenerle al sicuro da occhi indiscreti? Vediamolo insieme

## In parole semplici
Immaginiamo di avere tante chiavi che aprono tante porte diverse: una per la porta di casa, una per la cantina, una per la cassetta della posta, il garage, la macchina, la porta dell'ufficio ecc.

Immaginiamo ora invece tante copie della stessa chiave, che ha la capacità di aprire tutte le cose elencate prima. Come se le serrature fossero tutte uguali. 
  
Cosa succede quando perdiamo una chiave? Nella peggior delle ipotesi, nel primo esempio dobbiamo sostituire una serratura. Nel secondo, invece, dobbiamo sostituire *tutte* le serrature. Prima regola per una buona password: avere tante password.  
Se pensate che una password non sia importante quanto la chiave di casa vostra, provate a pensare a cosa succederebbe se qualche malintenzionato entrasse anche solo nel vostro account Facebook: potrebbe cancellare tutti i vostri contenuti (foto, post, video), spacciarsi per voi, postare contenuti vietati dal regolamento, vedere i vostri dati sensibili come il numero di telefono e, il danno maggiore, leggere e salvare tutte le vostre chat private. Non proprio cose da poco. 

Una cosa molto importante è anche *dove* lasciamo le chiavi. Mettiamo che, entrando in un bar, il barista ci dica: "se non vuoi rischiare di perdere le chiavi, te le posso tenere io nel retro". Senza contare che non potremmo mai sapere cosa possa effettivamente farci o non farci, perché di preciso dovremmo dare le nostre chiavi a uno sconosciuto? Per la stessa ragione, perché mai dovremmo usare strumenti che salvano le nostre password su un *cloud*, ovvero uno spazio internet? Questo vale sia per alcuni *password manager* che per tutti i moderni browser (Chrome, Firefox, Edge). E per Google.

In questo capitolo vediamo insieme più modi per costruire delle buone password, tenerle "in tasca" e persino ingannare chi dovesse riuscire ad accedervi.

## Cosa fare
> ATTENZIONE: dato che si parla di password e carte di credito, si invita a non seguire queste procedure con persone di cui non vi fidate nei dintorni

#### Rimozione password da internet
##### Google
Per prima cosa, sbarazziamoci di eventuali password salvate sull'account Google andando [qui](https://passwords.google.com/). Se non ne abbiamo, ottimo; in caso contrario, invece, armiamoci di *carta e penna* (quindi no, fare un documento sul PC non conta come "carta e penna") e clicchiamo su un qualsiasi sito nella lista.  
Per questioni di sicurezza, Google ci chiederà la nostra password Google. Inseriamola e ci troveremo in una pagina simile  
<div align="center"><img src="resources/L1-1 chromepwd.png"></div>    
La password è nascosta, quindi per leggerla premiamo sull'icona dell'occhio barrato. Una volta resa visibile, scriviamo sul foglietto:
1. nome sito
2. nome account
3. password

Dopodiché, premiamo su Elimina. Ripetiamo questo passaggio per *tutti* i siti, nessuno escluso. È di fondamentale importanza tenere traccia di ogni sito per ciò che vedremo in futuro, quindi __conserviamo con cura il foglietto__.

Inoltre, ancora più importante delle password, assicuriamoci di non avere i dati della nostra carta di credito salvati su Google andando [qui](https://pay.google.com/payments/home#paymentMethods) e, se utilizziamo Chrome, andando [qui](chrome://settings/payments). Nel primo caso basta premere su "rimuovi", nel secondo caso disabilitate l'opzione "Salva e compila i metodi di pagamento".

##### Browser internet
Tutti i browser moderni si offrono di salvare le nostre password quando le inseriamo: se usate Chrome le trovate [qui](chrome://settings/passwords). Se usate Firefox [qui](about:preferences#privacy) premendo sul tasto "Credenziali salvate".  
Prima di rimuoverle, segniamole su un foglietto come abbiamo fatto con Google qui sopra.

##### Password manager
Lo stesso discorso vale per chi utilizza un password manager come Keepass (se non sapete di cosa stiamo parlando, tranquilli: non ce l'avete).

#### Regole per una buona password
Vediamo ora, invece, le regole da seguire per una password sicura:
* Almeno 8 caratteri di lunghezza
* Utilizzare lettere maiuscole, minuscole, numeri, e simboli
* Non utilizzare variazioni del nostro nome, cognome, data di nascita, squadra preferita, eccetera
* Non utilizzare una parola comune (ad esempio "Motorino")
* Cercare di usare sempre password diverse
* Una password formata da 3-5 parole è più facile da ricordare e più difficile da violare rispetto a una singola parola scritta in modo strano

Quindi, per esempio, "AlabardaArena67Calciobalilla3" è più sicura di "P4$$w#rxD".
Inoltre, prima di utilizzare una password, assicuriamoci che questa non sia già nota in qualche database. Per farlo, c'è il sito [Have I Been Pwned](https://haveibeenpwned.com/Passwords) (la password non verrà inviata né memorizzata dal sito, tranquilli).  

#### Dove tenere le password
Iniziamo da dove **NON** tenerle:
* su un post-it incollato sullo schermo (molto comune negli uffici)
* su un documento sul PC
* su un cloud

Se non siete pratici con la tecnologia, il posto più sicuro è un foglio di carta. Un quaderno, un'agenda, un foglio volante, poco importa: l'importante è tenerlo al sicuro. Annotate quindi tutti i siti che avete frequentato con i rispettivi username, ma aspettate ancora un attimo a segnare le password (vi spiegheremo come camuffarle qui sotto).  
Invece, se avete dimestichezza con il PC e avete misure minime di sicurezza come una password allo sblocco (e possibilmente nessun keylogger *wink wink*), potete usare [KeePassX](https://www.keepassx.org/) che è un password manager locale. Segue una GIF, con password ovviamente sconsigliatissime :P e ricordatevi di salvare il database prima di chiudere!  
<div align="center"><img src="resources/L1-1 gif0.gif"></div>  
Infine, per gli utenti avanzati che vogliono un servizio in cloud, basta utilizzare prodotti come [BitWarden](https://bitwarden.com/) hostandosi il server a casa propria (basta un Raspberry Pi o simile). NON provateci se non sapete cosa state facendo.

#### Come camuffare le password
Vi mostriamo ora due modi coi quali, anche se una persona dovesse avere accesso al vostro foglietto (o al password manager), non riuscirebbe a combinare nulla.  
  
La prima strategia è quello che in gergo tecnico si chiama *salting*. Il salting consiste nell'aggiungere una parolina in più¹, sempre uguale, in tutte le vostre password SENZA scriverla sul foglio. 
Per esempio, mettiamo che la nostra parolina sia "SEDIA". Il foglio recita così (useremo password SCONSIGLIATE solo a scopo illustrativo):
> Facebook: ciao123  
Twitter: melanzana6  
Instagram: girandola90  

Se qualcuno prova ad accedere usando queste password, non ci riesce. Perché? Perché le vere password sono
> Facebook: ciao123SEDIA  
Twitter: melanzana6SEDIA  
Instagram: girandola90SEDIA

E il vantaggio è che dobbiamo ricordarci una sola parola. Ovviamente potevamo aggiungerla all'inizio (SEDIAciao123 ecc.), dopo due caratteri (ciSEDIAao123) ecc., a preferenza nostra.

Un'altra strategia, invece, è quella di cambiare alcuni caratteri. Per esempio, possiamo aumentare tutte le singole cifre di +1. Così, se sul foglio la nostra password è
> password247

Scriveremo
> password358 (abbiamo aggiunto +1 su ogni singolo numero. 2+1 = 3; 4+1 = 5; 7+1 = 8)

Bene: arrivati qui, sapete come avere password sicure, dove tenerle e addirittura come camuffarle. Vi ricordate il foglio dove vi era stato detto di non scrivere ancora le password? Perfetto, ora scrivete di fianco a ogni sito le NUOVE password che vorreste mettere su quel sito (camuffandole). Se proprio non v'interessa nulla di un certo sito e non avete sopra dati sensibili, ignoratelo ma NON rimuovetelo.  
Dulcis in fundo, bisogna cambiare le vecchie password con quelle nuove. Per farlo dovete ovviamente connettervi ai vari siti e modificarle dalle loro impostazioni; questa operazione può richiedere *più* o meno tempo a seconda di quante password dovete modificare, ma non demordete: fatto una volta, non dovrete più preoccuparvene :)

#### Sicurezza extra: autenticazione a due fattori (2FA, 2-factor authentication)
Avete presente le O-Key della banca che generavano un codice di 6 cifre prima di ogni passaggio importante? Ecco, stiamo parlando della stessa identica cosa.  
Molti siti hanno implementato l'autenticazione a due fattori (o *verifica in due passaggi*) ed è caldamente consigliata per quei siti sensibili che volete "blindare", come la posta (se non usate Gmail) o Facebook (abbiamo visto prima cosa può succedere).  
Per inviarci il codice come faceva la O-Key, ci sono due opzioni: via SMS e via app sul telefono. Cercate di evitare gli SMS (comporta dare il numero al servizio) e usate invece app come [AndOTP](https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&hl=en_US). Non possiamo purtroppo illustrare come impostare l'autenticazione per ogni sito in circolazione, ma vi basta cercare "2FA nomesito" per trovare facilmente qualche guida.  

Infine eccoci qui: quando si diceva di prendere il percorso con calma, ci si riferiva soprattutto a capitoli come questo. Una persona con 30 siti importanti ci metterà ovviamente più tempo rispetto a chi di importanti ne ha 5. Tuttavia, provate a vederla in maniera "zen": avete davvero bisogno di tutti quei siti? Ci torneremo tra qualche punto, quando inizieremo a fare pulizia :)
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente
[DA AGGIUNGERE: spiegazione crittografia browser e alternativa per password simili in vari gruppi, differenziandole con salting]  

## Approfondendo
[DA AGGIUNGERE: Ingegneria sociale, Strumenti amministratore di rete] 
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹in verità consiste nell'aggiungere caratteri in più, che siano lettere, numeri o simboli (tipo "9Qò("). Tuttavia, si è voluto semplificare per non rendere la frase troppo complessa.