# Fuga dal superfluo

> Quante parti di noi abbiamo lasciato fino a questo momento su internet, tra una registrazione e l'altra su siti di cui non ricordavamo neanche l'esistenza? Poco importa, perché è possibile riprendercele

## In parole semplici  

Intanto, se siete arrivati fin qui seguendo ogni capitolo, vogliamo farvi rendere conto dei passi fatti:  
*  Avete detto a Google di farsi un po' più gli affari suoi
*  Avete aumentato la sicurezza dei vostri account senza bisogno di avere password come 9sdA1l4#[F9d]ù0"klf_fka1£$L@àf
*  Avete detto a Facebook di farsi un po' più gli affari suoi
*  Avete rimosso dalla vostra abitazione dei mezzi che, oltre che potenziali cimici, sono incentivi alla pigrizia e mancato placebo per la solitudine
*  Avete imparato a rivalutare il valore delle conversazioni con le persone a voi care
*  Avete imparato che molti dei siti che usiamo ogni giorno ci intrappolano in camere dell'eco per farci credere di aver sempre ragione. E come evitarlo per i motori di ricerca
*  Avete risparmiato soldi su licenze Office (e detto a Microsoft di farsi un po' più gli affari suoi)

Se volete farvi un applauso, questo è il momento giusto :) e probabilmente, ognuno di voi ha imparato qualche lezione extra da tutto ciò: vi potremmo dire di raccontarcele sul gruppo Telegram, ma sono lezioni prima di tutto *vostre*: perché invece non condividerle con chi vi sta vicino? :)  
  
E ora, per la magia finale... allacciamoci le cinture, perché ne avremo bisogno!  
Avete presente Marie Kondō? La scrittrice giapponese diventata famosa per il suo metodo di riordinare casa e migliorare la qualità della propria vita¹? Bene, perché stiamo per fare esattamente la stessa cosa; ma con i nostri account superflui o che ci eravamo dimenticati di avere. E... beh, senza girarci molto intorno, c'è un'alta possibilità che ci richiederà tempo. *Tanto*.

Ogni volta che ci iscriviamo a un sito o a un'app, stiamo lasciando una traccia indelebile del nostro passaggio. Meglio ancora, stiamo lasciando dati sul nostro conto (falsi o meno). Lo facciamo senza accorgercene, magari ci registriamo in cambio di coupon, di documenti per studiare, di una newsletter e così via. Ma se poi ci guardiamo indietro e ci domandiamo: "Avevo davvero bisogno di quel servizio?", la risposta potrebbe non essere così scontata. Ed è così che alla fine ci ritroviamo interi indirizzi mail in disuso con 2000+ messaggi mai letti e che mai leggeremo (se non è il vostro caso, avete appena risparmiato 90% del tempo richiesto).  

Sembra paradossale, ma se abbiamo ancora accesso a quelle mail più simili a un cestino, è tempo di ringraziare il nostro pigro "me stesso". Perché? Perché è proprio grazie a quei rifiuti che possiamo ricostruire filo per segno la nostra storia online. E, grazie al GDPR -il regolamento europeo per la protezione dei dati-, andare su quei siti e cancellarli definitivamente. Perché? Per due motivi:
- ogni sito in meno equivale a una possibilità in meno che i nostri dati vengano venduti o rubati
- lo stesso motivo per cui Marie Kondō sistema casa: l'ordine mentale e la più facile gestione del tutto. Per una vita più serena

## Cosa fare

Iniziamo col *come* fare: piano piano. Questo capitolo richiede tempo e sforzi, e fare tutto in fretta non vi porterà ad altro se non a lasciare perdere dopo un paio di giorni. Per esempio, potete dedicarci 10 minuti al giorno.  

#### Occorrente
- foglietto delle password
- eventuale mail-spazzatura
- qualcuno di cui ci fidiamo con conoscenze sufficienti di internet se non siamo pratici

Prima di procedere, un'avvertenza: la cancellazione dell'account non funziona allo stesso modo per tutti i siti. Dato che non possiamo occuparci di ogni singolo sito (che è un po' come voler catalogare tutti i granelli di un pugno di sabbia), è qui che il "qualcuno di cui ci fidiamo" viene in nostro aiuto, se non siamo pratici. Più in basso abbiamo comunque elencato le varie tipologie di cancellazione. 

#### Preparativi

Come prima cosa ritiriamo fuori il famoso foglietto del capitolo delle password: se vi ricordate, vi avevamo detto di ignorare i siti di cui non vi importava più ma di tenerli segnati. Questo perché ora saranno i primi siti a fare ciao ciao.  
Oltre al foglietto, recuperiamo la nostra mail-spazzatura, entriamo e diamo un occhio alla posta in arrivo. Troveremo posta da più siti e quello che dobbiamo fare per ogni singolo sito è chiederci: "Mi serve/uso/mi ricordo di questo sito?". Se la risposta è no, quell'account può sparire. Niente sensi di colpa, niente forzarvi con "provo a recuperarne l'uso": se non l'avete usato fino ad ora, è perché non ne avevate bisogno. Ricordiamoci che l'idea è *togliere*, non aggiungere.

#### Come cancellare un account
- **tasto "Cancella account"**: questo è il caso più fortunato in quanto più breve. Basta fare il log-in nel sito e andare a cercare il tasto nelle impostazioni del profilo. Solitamente è scritto in piccolo, dato che è l'ultima cosa che (giustamente) i proprietari vorrebbero che faceste.
- **contatto mail/ticket**: se non trovate nessun tasto, cercate su internet "come cancellare account *nomesito*". Forse c'era e non l'avevate visto, o forse scoprirete che vogliono essere contattati via mail. Dovrete ovviamente usare la mail che avete usato per la registrazione. Siate concisi, tipo
> Buongiorno,  
vorrei il mio account e i miei dati cancellati dal vostro database. Il nickname è *mettete-il-nick* e ho usato questa mail per la registrazione. Sì, sono sicuro/a della scelta.  
Cordiali saluti

Ci metteranno circa un giorno per rispondervi, e potrebbe andarvi bene che accettino subito, oppure...
- **mail di conferma**: oppure vi chiederanno se siete sicuri della scelta. In quel caso rispondetegli confermando di nuovo. Potrebbero inoltre chiedervi dei dati per verificare che siete veramente voi, per esempio quando avete creato l'account, data di nascita e via dicendo. In casi rarissimi vi danno direttamente un modulo online da compilare per la cancellazione. Fatelo e aspettate un'eventuale mail di conferma
- **chiamata**: questo è il livello "perfido". In alcuni casi la mail non c'è e siete obbligati a chiamare. Come, per esempio, nel caso dell'EA (publisher videoludico).
- **newsletter**: può capitare invece di non essere registrati a dei siti ma solo alla loro newsletter (o a entrambi). Per cancellarvi dalla newsletter, apritene una e in fondo troverete il link per cancellarsi (in inglese "Unsubscribe"). Potreste dover seguire un altro paio di passaggi, come selezionare un motivo per il quale vi state cancellando, ma niente di ché.

#### All'opera
Ora non vi resta che darci sotto con le cancellazioni. Iniziate da quelle inutili del foglietto, e tirateci sopra una bella riga quando siete sicuri che l'account è stato rimosso. Per esempio, infatti, quando contattate un sito/app via mail, non viene cancellato in automatico: dovete aspettare la risposta di conferma. Prima di allora, non datelo per rimosso.  

Quando passerete ai siti della posta, di certo non potrete depennare lo schermo. Tuttavia, una volta confermata la cancellazione, potete usare lo strumento di ricerca della posta, cercare il sito da cui vi siete cancellati, e cestinarne tutte le mail. Fidatevi che vedere quel "posta in arrivo" ridursi sempre di più è anch'esso soddisfacente.  

Inoltre, se nei siti avete messo dati personali, come ulteriore protezione potreste cambiare quei dati prima di far sparire l'account. Non sappiamo dirvi quanto influisca, ma dato che richiede 2 secondi tanto vale provare.  

#### Siti ostici
Questa minisezione è per farvi un'idea dei siti più ostici. Oltre l'EA annunciata prima, abbiamo:
- giochi online. La maggior parte delle compagnie vi chiederà conferme nel dettaglio prima di cancellarvi
- Twitch. Nel profilo non appare nessun bottone per cancellare l'account né si riceve risposta ai ticket. Questo perché il link per cancellare l'account è nella policy sulla privacy: https://www.twitch.tv/user/delete_account

  
<br>
<br>
E infine, eccoci qui (probabilmente state sbirciando e non avete ancora iniziato, ma d'altronde è sempre meglio leggere fino in fondo :) ). Se tutto va "secondo i piani", vi ritroverete iscritti a neanche una decina di siti e/o newsletter (che tanto non avete il tempo di leggerle tutte). Magari vi ritroverete a non aver neanche più bisogno della mail-spazzatura... ma di questo ce ne occuperemo nel Livello 2 :)  
<br>
<br>
P.S.: la vostra privacy digitale è già molto più alta della media. Fatevi un altro applauso :D

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente

[DA AGGIUNGERE: GDPR]  

## Approfondendo

[DA AGGIUNGERE?]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice

¹ [Marie Kondō su Wikipedia](https://it.wikipedia.org/wiki/Marie_Kond%C5%8D)