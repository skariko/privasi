# Google - un esempio: vedere con i propri occhi i dati che abbiamo acconsentito di condividere

Quanto pensate condividiamo ogni giorno con certe applicazioni? Vediamolo, per esempio, con Google. <br>
Mentre leggete, cercate di rispondere a questa domanda: "**quante volte devo sbloccare il telefono per far sì che Google collezioni queste informazioni?**"
<br>
<br>
**Ingredienti**: un telefono Android senza modifiche; connessione dati attiva; GPS attivo.
<br>
<br>
**Il caso**: il nostro amico Carlo ci chiama per fare un giro.
<br>
<br>
**Cosa abbiamo fatto**: Risposto al telefono e accettato. Ci siamo trovati in un bar, fatto due passi per il centro visitando qualche negozio e verso sera ci siamo salutati.
<br>
<br>
**Cosa sa Google**: Carlo Morini (così lo abbiamo salvato in rubrica) ci ha chiamato alle 10:37 e siamo stati al telefono per 5 minuti e 7 secondi; alle 14:02 ci siamo diretti al Bar Tazzina in Via Garibaldi 9 impiegandoci 17 minuti a piedi e passando per Via Mazzini, Via dei Fiori e Via Leonardo; siamo rimasti fino alle 14:45 al bar, andando poi in centro e impiegandoci 20 minuti in macchina; parcheggiato, 7 minuti a piedi per arrivare in piazza, visitato Bricolage & Co. alle 15:25, Gelateria del Corso alle 16:10 e ImmobiliXTutti alle 17:00; alle 17:21 siamo tornati alla macchina, arrivando a casa alle 17:50. Ogni passo è stato tracciato.
<br>
<br>
Quante volte abbiamo sbloccato il telefono per fargli sapere tutto ciò? Tutte le volte che siamo entrati in un negozio? Tutte le volte che siamo saliti in macchina? Magari solo un paio di volte durante la giornata. Pensateci, secondo voi quante volte è stato necessario sbloccarlo per dargli certe informazioni? Sotto qualche riga c'è la soluzione.<br>
<br>
<br>
<br>
Ci avete provato? La risposta giusta è zero. Non abbiamo *mai* sbloccato il telefono durante la giornata. Lo abbiamo tenuto in tasca tutto il tempo. Niente app aperte, niente messaggi, niente ricerche, niente selfie, niente di niente. Ed è lo stesso Google a dircelo. <br>
Qui trovate le vostre attività: https://myactivity.google.com/myactivity  e qui i vostri spostamenti, se non avete negato il permesso al GPS: https://www.google.com/maps/timeline .<br>
Come vi fa sentire *vedere* questi dati rispetto a quando ne sentivate solo parlare?<br>
<br>
Dal Livello 1 vedremo come cancellarli e negare questi permessi.
<br>
#### Cos'è successo
Quello a cui abbiamo assistito si chiama *profilazione*. In altre parole, Google cerca di ottenere più informazioni possibili sul consumatore per proporgli pubblicità sempre più mirata. Parafrasando, cediamo la privacy per consigli su servizi che (forse) rispecchiano i nostri gusti.
<br>
<br>
Torniamo alle nostre porte. "Ti regaliamo una bella porta in legno massiccio!" recita un annuncio di un falegname. Dato che niente è gratis a questo mondo, qualcuno si chiederà dov'è la fregatura. Andando avanti leggiamo: "E vogliamo rovinarci! Installeremo anche telecamere e microfoni in ogni stanza, per migliorare la tua esperienza! Cosa vuoi di più?". Accettereste?<br>
Sembra assurdo, ma è lo stesso identico discorso: pagate per Gmail? Per Google Foto? Per Google Drive? Google Maps? Ancora meglio, c'è un solo servizio Google per il quale pagate di base? Cosa succederebbe invece se non pagaste per la luce, per internet, per l'acqua o per l'immondizia?<br>
Google non è una no-profit, anche i suoi dipendenti devono mangiare. E lo stesso discorso vale per Facebook, Instagram, Snapchat e qualsiasi altro servizio gratuito con un modello di business incentrato sulla profilazione.<br>
Non è la compagnia a essere il male assoluto, è il modello di business a essere dannoso. Considera le persone non come persone ma come numeri su cui fare incetta di dati in ogni singolo istante. E chi gli sta attorno poi inizia a imitarli: pensate alle testate giornalistiche e a come i titoli siano sempre più acchiappa-click. Questo perché si è barattata la qualità per la quantità, dove generare traffico per arrotondare (soldi spartiti con Google in cambio di profilazione) è per alcune testate più importante di informare. Risultato? Minore fiducia nei giornali (la gente non è stupida), minore informazione di qualità, più ignoranza¹. A voi l'immaginare i danni sul lungo corso.<br>
<br>
Attenzione, inoltre, a pensare che la profilazione esista solo su prodotti gratuiti: un caso è il pacchetto Microsoft Office 365 che, grazie a un'inchiesta del governo olandese, è risultato tracciare dati sensibili come e-mail scritte dentro i suoi programmi (Word, Excel ecc.)²  
<br>
<br>
Per concludere, queste aziende vengono definite *monopoli d'informazione*. Ed è per questo che si dice che "i dati sono la nuova valuta" o che "se è gratis, il prodotto sei tu".
<br>
Qui riportiamo i monopoli del 2012: la mappa indica il sito più usato in ogni stato³.<br>
![monopoli informazione](resources/Mappa mondiale dei siti dominanti.png)
<br>
<br>
[Livello 1: "Rimozione Consenso"](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-0___Activity-Deletion.md) <br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)
<br>
<br>
##### Fonti
¹Nalbone Daniele, Puliafito Alberto, [*Slow Journalism. Chi ha ucciso il giornalismo?*](http://www.fandangolibri.it/prodotto/slow-journalism/), Fandango Libri, Roma, 2019<br>
²Cimpanu Catalin, [*Dutch government report says Microsoft Office telemetry collection breaks GDPR*](https://www.zdnet.com/article/dutch-government-report-says-microsoft-office-telemetry-collection-breaks-gdpr/), ZDNet, 2018<br>
³Adiklis Vytautas, https://visual.ly/community/infographic/technology/world-map-dominating-websites, 2012
