# Conversazioni di tutti i giorni: ~~Messenger~~ Telegram
> È nato prima l'uovo o la gallina? Allo stesso modo, è meglio Whatsapp o Telegram? Beh, c'è gente che ha litigato per molto meno. La risposta è, semplicemente, che son due app diverse e avrebbe più senso paragonare Whatsapp con app come Signal (come vedremo nel prossimo capitolo) o Wire. Quindi, se proprio vogliamo fare un paragone, prendiamo un'altra app molto famosa: Facebook Messenger. È meglio Messenger o Telegram? 

## In parole semplici

Partiamo dalla cosa più banale: Messenger appartiene a Facebook.  

Vi sarà sicuramente capitato in questi ultimi anni di sentire che il social di Zuckerberg stesse avendo problemi con la privacy. Il motivo è molto semplice: Facebook, oltre a condividere lo stesso modello di business di Google, si è dimostrato irresponsabile nel tutelare i dati raccolti. Questi dati sono poi finiti nelle mani di persone che con l'azienda non c'entravano nulla, come Cambridge Analytica, e da lì a far la frittata tra implicazioni politiche e quant'altro è stato un attimo.
  
Parlando nello specifico di Messenger, un'inchiesta del New York Times ha dimostrato come Facebook abbia dato a Spotify, Netflix e alla Royal Bank of Canada il permesso di leggere, scrivere e cancellare i messaggi privati degli utenti¹. Al di fuori di uno scarica barile dove nessuno sapeva niente, il problema resta: qualcuno aveva accesso alle conversazioni private di qualsiasi utente, con inoltre la possibilità di modificarle e immedesimarsi in loro. Non importa se poi non lo hanno mai fatto, importa che era possibile farlo. E a proposito di accessi, fino a inizio agosto 2019 gli audio inviati su Messenger erano affidati a terze parti per essere trascritti (quindi ascoltati), senza il benché minimo avviso all'utente². Ironico, considerato che tre mesi prima Zuckerberg affermò: "Il futuro è privato".  

Audio a parte, lo stesso Zuckerberg ha dichiarato che la chat ha un sistema che scansiona automaticamente link e immagini prima di inviarle, per assicurarsi che non violino il regolamento di Facebook³. Ha poi precisato non esserci nessun essere umano a guardare questi messaggi -bensì un algoritmo-, tuttavia è utile sapere che la chat è, per definizione, monitorata.  

Quindi, qual è la funzione di Messenger? Potremmo dire che è un modo per comunicare con persone alle quali non si vuole dare il proprio numero. È per questo che app come Telegram esistono: per aggiungere qualcuno basta infatti il loro nickname. Inoltre, al contrario di app come WeChat, LINE e Viber, Telegram ci dice in parte cosa accade dietro le quinte (in gergo, *open source*). Questo è molto importante, perché non siamo più costretti a fidarci ciecamente: possiamo (chi se ne intende) sbirciare. 

Al contrario, per quanto Zuckerberg possa garantirci che l'app fa solo X e Y, l'unica scelta che abbiamo è quella di credergli o meno. D'altronde, perché non fidarsi di qualcuno che ha lasciato trapelare i dati di 87 *milioni* d'utenti con Cambridge Analytica e che ha fatto della profilazione il suo modello di business? Forse, è il momento di dire basta.

## Cosa fare
Come (quasi) ogni app, possiamo scaricare Telegram dallo store del nostro telefono. Facciamolo e, una volta scaricata, apriamola.  
  
Partirà un'installazione guidata dove ci verrà chiesto il nostro numero di telefono e un nickname. Fatto ciò... beh, fine. 

Dato che non vogliamo mostrare il nostro numero alle persone a cui abbiamo dato il nick, clicchiamo le tre lineette in alto a sinistra e nel menù a scomparsa premiamo su "Impostazioni".  
Lì andiamo su Privacy e Sicurezza e, premendo su Numero di telefono, mettiamo "Nessuno". Le altre opzioni invece impostatele a vostra discrezione.  

Torniamo poi indietro alle Impostazioni e, se vogliamo impostare un'immagine profilo, clicchiamo sull'icona della fotocamera. Potremo scegliere se scattarla sul momento o caricarla dalla galleria.  
<div align="center"><img src="resources/L1-2 pic0.png"></div>  

### Gruppo Telegram di Etica Digitale
Tra le tante funzioni di Telegram, c'è un sistema di ricerca per trovare persone, gruppi e canali (gruppi a senso unico dove si segue ciò che scrive il proprietario). Anche noi abbiamo pensato di fare un gruppo, dove siete tutti i benvenuti così da poter dare una mano e riflettere insieme sul percorso; o, più in generale, sull'etica digitale :)  
Per trovarlo, torniamo alla schermata principale e premiamo sulla lente d'ingrandimento.  
Si aprirà in automatico la barra di ricerca e lì scriviamo "etica digitale". Come primo risultato dovreste avere il nostro gruppo  
<div align="center"><img src="resources/L1-2 pic1.png"></div>  

Per entrare premeteci su: in basso noterete la scritta "Unisciti al gruppo". Premete e il gioco è fatto.  

### Regolamento
Si ricorda che, prima di scrivere, è opportuno leggere il regolamento. Lo trovate come "messaggio fissato" nella chat, ovvero nella parte alta dello schermo come da immagine. Se ci premete, vi porta al messaggio vero e proprio.  
<div align="center"><img src="resources/L1-2 pic2.png"></div>    
  
Prima di lasciarvi pasticciare (non nel gruppo ;) ma coi vostri amici ) con le varie funzioni di Telegram, è opportuno ricordare che il capitolo si chiama "Conversazioni di tutti i giorni". Questo perché vi **s**consigliamo di condividere informazioni troppo sensibili sull'app, per motivi che approfondiremo (per chi vuole) più in basso. Per quel tipo di informazioni, useremo Signal (c'è un capitolo apposito).  

### Versione Desktop
Telegram ha anche una comodissima versione per PC: la potete scaricare da [qui](https://desktop.telegram.org/)  

### Rimuovere Messenger
Ultimo ma non meno importante, disinstalliamo Messenger dal nostro telefono. Forse starete pensando che i vostri amici/familiari/colleghi/clienti sono lì e che non volete/potete lasciarli, ma rifletteteci un attimo: perché farsi del male se c'è una soluzione? Prima di tutto, se non potete proprio farne a meno, potete comunque continuare a usare la chat di Facebook dal computer. Questo è ottimo se avete clienti/datori di lavoro con i quali dovete rimanere in contatto e ai quali non volete dare il numero: perché fidatevi, la vostra sanità mentale non vuole che queste persone possano raggiungervi a qualsiasi ora del giorno o della notte sul telefono. Sul serio.    
Per gli amici e familiari il problema invece non si pone, perché si suppone abbiate i loro numeri e li sentiate già su app come WhatsApp o Signal.  
Tuttavia, c'è una soluzione ancora più semplice: perché non spiegare a queste persone quello che avete letto qui in alto? La privacy non è un insieme chiuso, funziona soltanto se anche le persone intorno a noi ne sono consapevoli. Che alla fine è il motivo per cui queste pagine esistono :)  
E perché poi, una volta che saranno su Telegram, il problema avrà smesso di esistere.  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente
[DA AGGIUNGERE: client open - server close; crittografia chat segrete]

## Approfondendo
[ACCETTASI SUGGERIMENTI CON ARGOMENTI INERENTI]  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹Dance Gabriel J.X., LaForgia Michael, Confessore Nicholas, [As Facebook Raised a Privacy Wall, It Carved an Opening for Tech Giants](https://www.nytimes.com/2018/12/18/technology/facebook-privacy.html), The New York Times, 2018  
²Frier Sarah, [Facebook Paid Contractors to Transcribe Users’ Audio Chats](https://www.bloomberg.com/news/articles/2019-08-13/facebook-paid-hundreds-of-contractors-to-transcribe-users-audio), Bloomberg, 2019  
³Ghoshal Abhimanyu, [Facebook confirms it spies on your Messenger conversations](https://thenextweb.com/facebook/2018/04/05/facebook-confirms-it-spies-on-your-messenger-conversations/), The Next Web, 2018