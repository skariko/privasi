# Conversazione sensibili pt.2: Signal incontra WhatsApp

> WhatsApp è l'app di messaggistica più usata al mondo, fungendo da vero e proprio social in certi stati. Quanto è privato? Ci sono alternative sulla piazza? E come è possibile anche solo immaginare che altre persone possano considerare altre opzioni rispetto a questo colosso di messaggistica?

## In parole semplici
Cosa rende una conversazione privata? Spesso c'è confusione tra cos'è anonimo e privato, quindi per capirlo meglio immaginiamo che ci siano due persone chiuse in una stanza a parlare.  
Se NON abbiamo visto chi sono ma sappiamo **cosa** si stanno dicendo, la conversazione è anonima. Al contrario, se NON sappiamo *cosa* stanno dicendo ma sappiamo *chi* c'è dentro la stanza, la conversazione è privata.  
Quindi, possiamo dire che una conversazione è privata quando solo gli interlocutori possono sentire/vedere cosa viene detto. Sotto questo punto di vista, sia WhatsApp che Signal sono private.

Mettiamo, ora, che prima di entrare nella stanza ci sia una telecamera per vedere chi entra, quando, come e con chi: analizzando la registrazione si possono quindi capire le abitudini, con chi si parla di più, quando si è soliti entrare ecc. facendosi un'idea della persona.  
In informatica, chiamiamo questi dati di contorno *metadati*. E proprio i metadati son quelli che WhatsApp da contratto¹ passa a Facebook e a terze parti, per una migliore profilazione (ultimo accesso, quanto state connessi, con chi chattate di più, numero di telefono, numeri in rubrica, connessione internet, posizione GPS ecc.). A livello di privacy, è come dirvi che non entrerà mai in casa vostra sfondando la porta, ma non fa accenno al fatto che passi indisturbato per la finestra, lucrandoci sopra.  
Inoltre vi potreste chiedere: "Perché a Facebook *e* a terze parti?". Perché Whatsapp *è* di Facebook dal 2014², come lo è Instagram dal 2012³.  


Vi sarà inoltre capitato di leggere che le chat di WhatsApp sono protette da "crittografia end-to-end". In parole povere, è un modo per impedire alla gente esterna (azienda inclusa) di poterle leggere. Tuttavia una cosa rimane senza questa protezione: il *backup* dei messaggi. Il backup (ovvero la copia dei messaggi) viene fatto su iCloud se si ha un iPhone, o Google Drive se si ha Android. In altre parole, le vostre conversazioni sono salvate *in chiaro* su un servizio di terze parti. Vi ricordate il barista che si offre di tenervi le chiavi? Esattamente lo stesso discorso.  

Infine, proprio come Messenger, WhatsApp non è *open source*, privandoci di sapere cosa succede dietro le quinte. Se aggiungiamo poi come se la passa Facebook con la privacy e l'onestà, riteniamo sia un ulteriore motivo per non affidarcisi.  

"Sì, ma tutti quelli che conosco usano WhatsApp". E avete ragione: isolarsi dal mondo sarebbe stupido.  
È per questo che infatti vi consigliamo di AFFIANCARE l'app di Signal a Whatsapp. Signal ha la stessa protezione, con l'aggiunta che non colleziona metadati, è completamente open source (chi è del mestiere può controllare che non abbia fregature), non manda backup a terze parti mettendo a rischio la vostra privacy, ha un'intera community di supporto⁴ e, inoltre, vi permette di poter leggere e scrivere anche SMS (come unire WhatsApp e SMS nella stessa app). Per intenderci, viene usata persino dai giornalisti in territori come la Siria per evitare di venire rintracciati dai governi locali.

## Cosa fare
Come Telegram, trovate Signal sullo store delle app. Installatela, seguite le istruzioni, dategli il permesso per gli SMS e... fine!  

Veniamo invece al lato sociale, quello più importante: chi usa Signal a parte me?  
Quando provate a scrivere a qualcuno, se la sua iniziale nella rubrica di Signal è azzurra vuol dire che ha l'app come voi. In caso contrario, scrivergli equivarrà a scrivere un SMS (che non è criptato). Quindi, se qualcuno non è sull'app, ci si potrebbe chiedere: come porto la gente su Signal (o qualsiasi altra app)?  
Ma, se ci si pensa su, non è propriamente la domanda corretta. La domanda corretta è invece *perché* dovrei portare qualcuno su Signal.  

Potremmo dirvi di parlarne con chiunque vi capiti a tiro per convincerli, ma la verità è che risultereste pesanti perché mettersi uno stendardo sulla schiena e andarci in giro costantemente, ci farà risultare quanto meno strani. Certo, potete accennarlo, ma se non volete diventare lo stereotipo del vegano fastidioso (che poi mette in cattiva luce tutti gli altri vegani), non insistete.  

Pensiamo più in piccolo: parliamone con le persone che *ci stanno a cuore*. Per esempio, con i nostri genitori (più propensi ad ascoltarci di uno sconosciuto, soprattutto se non hanno dimestichezza con la tecnologia), il nostro partner o l'amico d'infanzia. Questo perché sono persone alle quali teniamo e le quali non dovrebbero avere problemi a scaricare un'app in più per noi. E, ancora più importante, sono persone con le quali abbiamo un rapporto più intimo, che non dovremmo voler "contaminato" da aziende sulle quali potremmo speculare all'infinito senza aver risposte (finché non salta fuori l'ennesima inchiesta). E state pur sicuri che a loro volta avranno altre persone importanti alle quali dirlo :)  

Infine, ora che avete sia un'app per le conversazioni "pubbliche" e una per quelle più sensibili, sta a voi decidere quando e quando usare una o l'altra: chiedetevi quando scrivete: "quello che sto per dire contiene dati sensibili (per esempio dati personali, foto dei vostri figli -che vi consiglieremmo a prescindere di non inviare né caricare su internet-, documenti aziendali ecc.), o è un'informazione generica?"

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente
[DA AGGIUNGERE]  

## Approfondendo
[DA AGGIUNGERE]  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ Contratto WhatsApp: https://www.whatsapp.com/legal/?eea=1&lang=it  
² Covert Adrian, [Facebook buys WhatsApp for $19 billion](https://money.cnn.com/2014/02/19/technology/social/facebook-whatsapp/index.html), CNN, 2014  
³ Rusli M. Evelyn, [Facebook Buys Instagram for $1 Billion](https://dealbook.nytimes.com/2012/04/09/facebook-buys-instagram-for-1-billion/), New York Times, 2012  
⁴ https://community.signalusers.org/
