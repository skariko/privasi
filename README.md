<div align="center">[in allestimento]</div>

# PrivaSì

Se pensi di non aver nulla da nascondere, sei nel posto giusto: PrivaSì, infatti, nasce per analizzare il discorso privacy da più punti di vista. Non solo quello di sicurezza, bensì anche quello etico, più umano. Per esempio, la domanda che forse dovresti porti non è "cosa mi importa se tanto non ho nulla da nascondere?" ma "perché dovrei condividere con una macchina cose che, se sommate, non direi neanche alla persona più cara?".

Attraverso un percorso passo dopo passo, PrivaSì si propone di rispondere a questo e altri quesiti mentre guida la persona nell'utilizzo di mezzi più etici, non invasivi, accompagnandola dall'installazione fino ad eventuali modifiche. Ci si tiene a sottolineare che *i quesiti* sono il fattore chiave, perché seguire istruzioni senza capirne il senso avrebbe sul lungo corso un effetto controproducente. Ed è proprio per questo che le spiegazioni vengono sempre *prima* della guida vera e propria. Capire, prima di fare.

Si è anche consci che non tutti masticano informatica. Onde per cui ogni passo del percorso contiene sia una spiegazione più terra terra, una un po' più tecnica e infine una ancora più approfondita; per accontentare tutti i palati, oltre che per il dovere d'informazione.

Si ricorda inoltre che il progetto è Open Source, ovvero chiunque è libero di contribuire alla sua stesura.  
Per ulteriori domande, consultate le [FAQ](https://gitlab.com/etica-digitale/privasi/blob/master/FAQ.md) (domande più frequenti).

[INIZIA IL PERCORSO](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)