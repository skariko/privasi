> **!!!COME USARE LA LISTA!!!**
> Come dice il detto: "chi va piano, va sano e va lontano". L'obiettivo non è arrivare primi (non c'è nessun premio), bensì capire *cosa* fa il singolo punto e il *perché* è stato suggerito. Cliccateci su e si apriranno le istruzioni con una breve spiegazione (ma anche lunga, per chi vuole approfondire). Prendetevi il tempo che vi serve :)

> **!!!come NON usare la lista!!!**
> Seguire ogni punto senza capirne il motivo. Chiudercisi in maniera compulsiva finché non si ha finito. Saltare tra i livelli

**!!! La lista non è ultimata: chiunque è libero di contribuire forkando o aprendo una issue riguardo i punti già esistenti (quelli cliccabili) !!!**  

**!!! I lavori procedono quotidianamente: se siete arrivati all'ultimo punto cliccabile, vi invitiamo a fare un salto una volta a settimana per trovare nuovi punti consultabili. [QUI](https://gitlab.com/etica-digitale/privasi/commits/master) il registro di tutte le modifiche !!!**  

<br>

**Livello 0: Comprendere**
- [Introduzione: cosa me ne importa della privacy?](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L0-0___Intro.md)
- [Google - un esempio: vedere con i propri occhi i dati che abbiamo acconsentito di condividere](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L0-1___Google.md)

&nbsp;

**Livello 1: Limitare i danni**
- [Rimozione consenso](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-0___Activity-Deletion.md)
- [Password 101](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-1___Passwords.md)
- [Chat pubbliche: ~~Messenger~~ Telegram ](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-2___Telegram.md)
- [Conversazioni sensibili pt.1: Smart TV e assistenti personali (Internet delle Cose)](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-3___IoT.md)
- [Conversazione sensibili pt.2: Signal incontra Whatsapp](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-4___Signal.md)
- [Motori di ricerca: al di là di Google](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-5___Search-Engine.md)
- [LibreOffice](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-6___Libre-Office.md)  
- [Gesti quotidiani](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-7___Daily-Habits.md)
- [Fuga dal superfluo](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-8___Accounts-deletion.md)

&nbsp;

**Livello 2: Mezzi per muoversi**
- [Cookie: briciole di internet](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L2-0___Cookies.md)
- F-Droid: store alternativi
- ~~Chrome~~ Firefox anonimo (PC) e Firefox Klar (telefono)
- Mail temporanee
- ~~YouTube~~ Invidious e NewPipe
- ~~Google Maps~~ OpenStreetMap
- WebApps: social a compartimenti stagni
- Rimozione contenuti da Google (Foto, Drive)

&nbsp;

**Livello 3: Compartimentare**
- Compartiche?
- Personale: Tor
- Social e Professionale: profili Mozilla
- uBlock Origin
- Mail separate

&nbsp;

**Livello 4: Pilastri del quotidiano**
- ~~Spotify~~
- ~~WhatsApp~~
- ~~Discord~~
- Social: tra il fediverso e il nulla
- Streaming on-demand: dimmi cosa guardi e ti dirò chi sei
- Alternative etiche: una lista

&nbsp;

**Livello 5: Chi fa da sè**
- Freemium e nuovi modelli di business
- ROM telefono
- GNU/Linux: il sistema operativo libero (che non vuole migliorare la tua esperienza)

Da inserire: DNS
